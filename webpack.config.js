const Encore = require('@symfony/webpack-encore'),
    AppBundle = './src/AppBundle/Resources/assets/',
    UserBundle = './src/UserBundle/Resources/assets/';

Encore
    // directory where all compiled assets will be stored
    .setOutputPath('web/build/')

    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/build')

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // STYLES
    .addStyleEntry('css/vendor',    AppBundle + 'scss/vendor.scss')
    .addStyleEntry('css/main',      AppBundle + 'scss/main.scss')
    .addStyleEntry('css/form',      UserBundle + 'scss/form.scss')

    // JS
    .addEntry('js/vendor',          AppBundle + 'js/vendor.js')
    .addEntry('js/main',            AppBundle + 'js/main.js')
    .addEntry('js/form',            UserBundle + 'js/form.js')

    // allow sass/scss files to be process
    .enableSassLoader()

    // generate source map in Dev mode
    .enableSourceMaps(!Encore.isProduction());

// export the final configuration
module.exports = Encore.getWebpackConfig();