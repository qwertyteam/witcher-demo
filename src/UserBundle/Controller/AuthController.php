<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use UserBundle\Form\UserLoginType;
use UserBundle\Form\UserType;
use UserBundle\Entity\User;


/**
 * Class AuthController - User authorization controller
 * @package UserBundle\Controller
 */
class AuthController extends Controller
{

    /**
     * User registration page
     *
     * @Route("/register", name="user_registration")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        // redirect if user logged
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('homepage');
        }

        // create register form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // Form POST method
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // password encoder interface
            $passwordEncoder = $this->get('security.password_encoder');

            // password encode
            $password = $passwordEncoder->encodePassword($user, $user->getPasswordForm());
            $user->setPassword($password);

            // set user role
            $user->setRoles(['ROLE_USER']);

            // save user
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // login after registration
            $token = new UsernamePasswordToken(
                $user,
                $password,
                'main',
                $user->getRoles()
            );
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));

            // Redirect to homepage
            return $this->redirectToRoute('homepage');
        }

        return $this->render('UserBundle:Auth:register.html.twig', array(
            'title' => 'User registration',
            'form' => $form->createView()
        ));
    }


    /**
     * User login page
     *
     * @Route("/login", name="user_login")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        // redirect if user logged
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('homepage');
        }

        // auth utils
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login base error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last email entered by the user
        $lastUserName = $authenticationUtils->getLastUsername();

        // create login form
        $form = $this->createForm(UserLoginType::class, ['_username' => $lastUserName]);

        return $this->render('UserBundle:Auth:login.html.twig', array(
            'title' => 'Sing in',
            'form' => $form->createView(),
            'error' => $error
        ));
    }


    /**
     * User logout action
     *
     * @Route("/logout", name="user_logout")
     */
    public function logoutAction()
    {

    }

}
