<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Tweet;


/**
 * Class DefaultController
 *
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{

    /**
     * Homepage action
     *
     * @Route("/", name="homepage")
     * @throws \Exception
     */
    public function indexAction()
    {
        $twitterHandler = $this->get('app.tweet');
        $tweets = $twitterHandler->getTweets();

        // replace this example code with whatever you need
        return $this->render('AppBundle:Default:index.html.twig', [
            'tweets' => $tweets
        ]);
    }


    /**
     * Get new tweets list
     *
     * @Route("tweets-load", name="tweets-load", methods="POST")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function tweetsAjaxLoadAction(Request $request)
    {
        $max_id = $request->request->get('max_id');

        if ($max_id && (is_numeric($max_id) && is_string($max_id))) {
            $twitterHandler = $this->get('app.tweet');
            $tweets = $twitterHandler->getTweets($max_id);

            return new JsonResponse(array(
                'tweets' => $tweets,
                'max_id' => $max_id
            ));
        }

        return new JsonResponse(array('error' => 'Not has max_id'));
    }


    /**
     * Tweet Like action
     *
     * @Route("tweets-like", name="tweets-like", methods="POST")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function tweetLikeAction(Request $request)
    {
        $user = $this->getUser();

        // user not logged
        if (!$user) {
            return new JsonResponse(array('error' => 'Please, login to website!'));
        }

        $data = array(
            'id'            => $request->request->get('id'),
            'user_name'     => $request->request->get('user_name'),
            'url'           => $request->request->get('url')
        );

        // tweet data: complete validation
        if (!$data['id'] || !$data['user_name'] || !$data['url']) {
            return new JsonResponse(array('error' => 'Not complete tweet data'));
        }

        // save data
        $save = $this->getDoctrine()->getRepository(Tweet::class)->like($data, $user);

        return new JsonResponse(array(
            'result' => $save['result'],
            'error'  => $save['error'] ?? false,
            'count'  => $save['count']
        ));
    }


    /**
     * Export tweets like report
     *
     * @Route("export", name="export")
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     * @throws \PHPExcel_Exception
     */
    public function exportAction ()
    {
        $tweets = $this->getDoctrine()->getRepository('AppBundle:Tweet')->getAllTweetsLikes();

        // ask the service for a Excel5
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->getProperties()->setCreator("liuggio")
            ->setTitle("The Witcher tweets report")
            ->setSubject("The Witcher tweets report");

        // titles
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'User name')
            ->setCellValue('B1', 'Link')
            ->setCellValue('C1', 'Likes');

        // values
        $count = 2;
        foreach ($tweets as $tweet) {
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$count, $tweet['userName'])
                ->setCellValue('B'.$count, $tweet['url'])
                ->setCellValue('C'.$count, count($tweet['users']));
            $count++;
        }

        // Set active sheet
        $phpExcelObject->setActiveSheetIndex(0);

        // Set sheet title
        $phpExcelObject->getActiveSheet()->setTitle('Tweets');

        // columns auto size
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

        // add borders
        $styleArray = array(
            'borders' => array(
                'allborders' => array( 'style' => \PHPExcel_Style_Border::BORDER_THIN )
            )
        );
        $phpExcelObject->getActiveSheet()->getStyle('A1:C'. --$count)->applyFromArray($styleArray);
        unset($styleArray);

        // header background color
        $phpExcelObject->getActiveSheet()->getStyle('A1:C1')->applyFromArray(
            array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'F28A8C')
                )
            )
        );

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');

        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);

        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'The-witcher-tweets-report'. date("d-m-Y_Y-G-i", time()) .'.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

}
