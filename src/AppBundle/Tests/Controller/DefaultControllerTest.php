<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Controller\DefaultController;
use Symfony\Component\HttpFoundation\Request;

class DefaultControllerTest extends WebTestCase
{
    
    /**
     * Index request status and HTML elements test
     */
    public function testIndexHtml()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $this->assertContains('The Witcher  Tweet', $crawler->filter('.top-bar .logo h1')->text());
        
        $this->assertSame(true, $crawler->filter('h3')->count() > 5); // count(h3) > 5
        
        // $this->assertContains('Hello World', $client->getResponse()->getContent());
    }
    
    
    /**
     * @dataProvider tweetsAjaxLoaderData
     * 
     * @param string $key
     * @param string $max_id
     */
    public function testTweetsAjaxLoader($key, $max_id)
    {        
        // create request
        $request = new Request();
        $request->setMethod('POST');
        $request->request->set('max_id', $max_id);

        // create container
        $client = static::createClient();
        $container = $client->getContainer();
        
        // controller action
        $controller = new DefaultController();
        $controller->setContainer($container);
        $response = $controller->tweetsAjaxLoadAction($request);
        
        // TEST: check response content
        $this->assertObjectHasAttribute('content', $response); // Check response content
        
        $json = $response->getContent();
        $data = json_decode($json, true);
        
        switch ($key) {
            case 'real':
                $this->assertArrayHasKey('tweets', $data);
                break;
            default:
                $this->assertArrayHasKey('error', $data);
                break;
        }
    }
    
    
    /**
     * Ajax tweets loader max_id values
     * 
     * @return array
     */
    public function tweetsAjaxLoaderData()
    {
        return [
            'empty'      => ['empty', null],
            'numeric'    => ['numeric', 123456789],
            'text'       => ['text', 'xaxaxaver'],
            'bool'       => ['bool', 1.355513232],
            'real'       => ['real', '1051329374111354881']
        ];
    }

}
