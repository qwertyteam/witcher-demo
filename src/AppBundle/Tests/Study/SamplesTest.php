<?php

/*
 * PHPUnit study test samples
 */

namespace AppBundle\Tests\Study;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Description of TestSamplesTest
 *
 * @author V. Shelelo
 */
class TestSamplesTest extends WebTestCase {
    
    /**
     * Check true value
     */
    public function testTrueIsTrue()
    {
       $foo = true;
       $this->assertTrue($foo);
    }
    
    /**
     * Value === Value
     */
    public function testSameValues()
    {
        $this->assertSame(1, 1); // 1 === 1
        $this->assertSame('test', 'test'); // 'test' === 'test'
    }
    
    /**
     * Dependence test (first): create empty array
     * @return array
     */
    public function testDependenceFirst ()
    {
        $arrayStack = ['test'];
        $this->assertNotEmpty($arrayStack); // is Empty
        
        return $arrayStack;
    }
    
    /**
     * Dependence test (second): add value to array
     * 
     * @depends testDependenceFirst
     * @param array $arrayStack
     */
    public function testDependenceSecond (array $arrayStack)
    {
        array_push($arrayStack, 'value');
        $this->assertSame('value', $arrayStack[count($arrayStack) - 1]); // value === value
        $this->assertContains('test', $arrayStack); // if has 'test' in array
        $this->assertNotEmpty($arrayStack); // is not Empty
    }
    
    /**
     * Base Add test - data provider
     * 
     * @return array
     */
    public function dataProviderAddTest ()
    {
        return [
            'one plus one' => [1, 1, 2],
            'one plus two' => [1, 2, 3]
        ];
    }
    
    /**
     * Add two numer by data provider 
     * 
     * @dataProvider dataProviderAddTest
     * @param int $a
     * @param int $b
     * @param int $result
     */
    public function testBaseAdd ($a, $b, $result)
    {
        $this->assertSame($result, $a + $b);
    }
    
}
