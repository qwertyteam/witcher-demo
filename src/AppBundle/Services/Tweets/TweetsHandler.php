<?php

namespace AppBundle\Services\Tweets;

use TwitterAPIExchange;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;


/**
 * Class TweetsHandler
 *
 * @package AppBundle\Services\Tweets
 */
class TweetsHandler
{

    /**
     * Twitter API settings
     *
     * @var array
     */
    private $settings;


    /**
     * Count tweets for one request
     *
     * @var int
     */
    private $count;


    /**
     * @var EntityManager
     */
    private $em;


    /**
     * TweetsHandler constructor.
     *
     * @param Container $container
     * @param EntityManager $em
     */
    public function __construct(Container $container, EntityManager $em)
    {
        $this->settings = array(
            'oauth_access_token'        => $container->getParameter('twitter_api.oauth_access_token'),
            'oauth_access_token_secret' => $container->getParameter('twitter_api.oauth_access_token_secret'),
            'consumer_key'              => $container->getParameter('twitter_api.consumer_key'),
            'consumer_secret'           => $container->getParameter('twitter_api.consumer_secret'),
        );

        $this->em = $em;
        $this->count = 60;
    }


    /**
     * Get tweets with using REST API
     *
     * @param bool $max_id
     * @return array
     * @throws \Exception
     */
    public function getTweets($max_id = false)
    {
        // Request URL
        $url = 'https://api.twitter.com/1.1/search/tweets.json';

        // Request method
        $requestMethod = 'GET';

        // Search Fields
        $getField = '?q=#witcher';
        // $getField .= ' +filter:images';
        $getField .= ' +filter:media';
        $getField .= ' -filter:nativeretweets';
        $getField .= '&include_entities=true';
        $getField .= '&result_type=recent';
        $getField .= '&count='. $this->count;
        // $getField .= '&src=typd';
        if ($max_id) {
            $getField .= '&max_id='. $max_id;
        }

        // Get tweets
        $twitter = new TwitterAPIExchange($this->settings);
        $json = $twitter
            ->setGetfield($getField)
            ->buildOauth($url, $requestMethod)
            ->performRequest(true, array(CURLOPT_CAINFO => dirname(__FILE__) .'/cacert-2018-06-20.pem'));

        return $this->tweetsOutput(json_decode($json));
    }


    /**
     * Build tweets list
     *
     * @param object $tweets
     * @return array
     */
    private function tweetsOutput($tweets)
    {
        if (!isset($tweets->statuses) || count($tweets->statuses) < 1) {
            return array();
        }

        $output = array();

        // all tweets likes
        $em = $this->em->getRepository('AppBundle:Tweet');
        $likes = $em->getAllTweetsLikesCount();

        foreach ($tweets->statuses as $tweet) {
            // Tweet url
            $hasUrl = isset($tweet->entities->media[0]->url) ? $tweet->entities->media[0]->url : false;
            if (!$hasUrl) {
                $hasUrl = isset($tweet->entities->urls[0]->url) ? $tweet->entities->urls[0]->url : false;
            }

            // Skip if not has URL
            if (!$hasUrl) { continue; }

            // Video (gif)
            $hasVideo = isset($tweet->extended_entities->media[0]->video_info->variants[0]->url)
                        ? $tweet->extended_entities->media[0]->video_info->variants[0]->url
                        : false;

            // Image
            $hasImage = isset($tweet->entities->media[0]->media_url) ? $tweet->entities->media[0]->media_url : false;

            // Skip if not has Image || GIF
            if (!$hasVideo && !$hasImage) { continue; }

            if ($hasUrl && ($hasImage || $hasVideo)) {
                $output[] = array(
                    'id'            => $tweet->id_str,
                    'text'          => $tweet->text,
                    'image'         => $hasImage,
                    'video'         => $hasVideo,
                    'url'           => $hasUrl,
                    'user_name'     => $tweet->user->name,
                    'user_url'      => 'https://twitter.com/'.$tweet->user->screen_name,
                    'user_photo'    => $tweet->user->profile_image_url,
                    'created_at'    => date('d-m-Y H:i:s', strtotime($tweet->created_at)),
                    'likes'         => $likes[$tweet->id_str] ?? 0
                );
            }
        }

        return $output;
    }

}
