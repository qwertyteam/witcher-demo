websitePreLoadingInit();

/**
 * Init website pre-loader
 */
function websitePreLoadingInit() {
    let $loaderWrap = document.createElement('div');
    $loaderWrap.setAttribute('id', 'loader-wrapper');
    $loaderWrap.innerHTML = '<div class="loader">... loading ...</div>';
    document.body.appendChild($loaderWrap);

    // remove action
    window.addEventListener('load', websitePreLoadingRemove)
}

/**
 * Remove pre-loader after window load
 */
function websitePreLoadingRemove() {
    // remove loader
    let loader = document.getElementById('loader-wrapper');
    loader.parentNode.removeChild(loader);

    // remove js loader
    let js = document.querySelector('[data-id="app-preload-js"]');
    js.parentNode.removeChild(js);
}