window.onload = function () {

    const $tweetsList = document.querySelector('.tweets-list');

    // Init homepage actions
    if ($tweetsList) {
        tweetsActions($tweetsList);
        likeTweets($tweetsList);
    }

};


/**
 * Homepage tweets actions
 *
 * @param {object} $tweetsList
 */
function tweetsActions ($tweetsList)
{

    /**
     * Pre-loader HTML
     *
     * @type {string}
     */
    const $loaderHtml = '<div class="loader">... loading ...</div>';


    /**
     * Masonry grid object
     */
    const $msnry = new Masonry( $tweetsList, { itemSelector: '.tweet-wrap' });


    /**
     * New tweets aJax URL
     *
     * @type {string}
     */
    const $ajaxUrl = document.querySelector('meta[name="tweets-load-url"]').getAttribute('content');


    /**
     * Last tweet ID
     *
     * @type {string}
     */
    let $max_id = $tweetsList.lastElementChild.firstElementChild.getAttribute('data-id');


    /**
     * Start autoload bottom position
     *
     * @type {number}
     */
    let $autoloadPoint = window.innerHeight * 2;


    /**
     * Loader action blocker
     *
     * @type {boolean}
     */
    let $loaderAction = false;


    /**
     * Not has new tweets indicator
     *
     * @type {boolean}
     */
    let $finishLoad = false;


    /**
     * Parse HTML new tweets and insert to list
     *
     * @param {array} $tweets
     */
    function parseNewTweetsHtml ($tweets)
    {
        const $template = $tweetsList.lastElementChild;
        const $lastCheck = $tweets.length - 2;

        for (let $index in $tweets) {
            // skip the first element (duplicate)
            if ($index < 1) { continue; }

            // create clone
            let $item = $tweets[$index],
                $last = $index > $lastCheck,
                $clone = $template.cloneNode(true);

            // update clone data
            $clone.querySelector('.tweet').setAttribute('data-id', $item['id']);
            $clone.querySelector('.author a').innerHTML = $item['user_name'];
            $clone.querySelector('.author img').src = $item['user_photo'];
            $clone.querySelector('.author img').alt = $item['user_name'];
            $clone.querySelector('.author a').href = $item['user_url'];
            $clone.querySelector('.date span').innerHTML = $item['created_at'];
            $clone.querySelector('h3 a').innerHTML = $item['text'];
            $clone.querySelector('h3 a').href = $item['url'];
            $clone.querySelector('.img-body a').href = $item['url'];
            $clone.querySelector('.img-body a').innerHTML = "";

            // create new image
            let $img = new Image();
            $img.src = $item['image'];
            $img.alt = 'tweet '+ $item['id'];

            // has video
            if ($item['video']) {
                // create new video
                let $video = document.createElement("video");
                $video.muted = true;
                $video.autoplay = true;
                $video.loop = true;
                $video.poster = $item['image'];
                $video.src = $item['video'];
                $video.setAttribute('type', 'video/mp4');
                $video.appendChild($img);

                $clone.querySelector('.img-body a').appendChild($video);
                $tweetsList.appendChild($clone);
                $msnry.appended($clone);
                $video.addEventListener('loadeddata', function() {
                    $msnry.layout();
                }, false);
            }

            // has image
            else {
                $clone.querySelector('.img-body a').appendChild($img);
                $tweetsList.appendChild($clone);
                $msnry.appended($clone);
                $img.addEventListener('load', function() {
                    $msnry.layout();
                }, false);
            }

            // update max_id & disable loader action
            if ($last) {
                $max_id = $item['id'];

                $loaderAction = false;

                let $element = document.querySelector('.loader');
                $element.parentNode.removeChild($element);
            }
        }
    }


    /**
     * Get new tweets: send Ajax request
     */
    function tweetsRunAjax ()
    {
        // init loader action
        $loaderAction = true;
        $tweetsList.insertAdjacentHTML('afterend', $loaderHtml);

        // create a new XMLHttpRequest object
        let $request = new XMLHttpRequest();

        // POST params
        let $params = new FormData();
        $params.append('max_id', $max_id);

        // open the request and pass the HTTP method name and the resource as parameters
        $request.open('POST', $ajaxUrl, true);

        // ajax response
        $request.onreadystatechange = function () {
            if ($request.readyState === 4 && $request.status === 200) {
                let $response = JSON.parse(this.responseText);

                // error alert
                if ($response.error) {
                    alert($response.error);
                }

                // insert tweets
                if ($response.tweets.length > 1) {
                    parseNewTweetsHtml ($response.tweets);
                }

                // tweets finish
                else {
                    // not has tweets
                    $finishLoad = true;

                    // remove pre-loader
                    let $element = document.querySelector('.loader');
                    $element.parentNode.removeChild($element);

                    // insert info
                    $tweetsList.insertAdjacentHTML('afterend', "<h2><span>It's all &#9786;</span> <br> In the Standard search API is available tweets no older than 10 days!</h2>");
                }
            }
        };

        // send request
        $request.send($params);
    }


    // Window scrolling
    window.addEventListener('scroll', function() {
        let $startPoint = $tweetsList.offsetHeight - $autoloadPoint;

        if ($finishLoad === false && $loaderAction === false && $startPoint < window.scrollY) {
            tweetsRunAjax ();
        }
    });


    // Window resize
    window.addEventListener('resize', function() {
        $autoloadPoint = window.innerHeight * 2;
    });

}


/**
 * Like action
 *
 * @param {object} $tweetsList
 */
function likeTweets($tweetsList)
{

    /**
     * New tweets aJax URL
     *
     * @type {string}
     */
    const $url = document.querySelector('meta[name="tweets-like-url"]').getAttribute('content');


    /**
     * Send like to backend
     *
     * @param {string} $id
     * @param {string} $username
     * @param {string} $link
     * @param {object} $countElement
     */
    function sendLike ($id, $username, $link, $countElement)
    {
        // create a new XMLHttpRequest object
        let $request = new XMLHttpRequest();

        // POST params
        let $params = new FormData();
        $params.append('id', $id);
        $params.append('user_name', $username);
        $params.append('url', $link);

        // open the request and pass the HTTP method name and the resource as parameters
        $request.open('POST', $url, true);

        // ajax response
        $request.onreadystatechange = function () {
            if ($request.readyState === 4 && $request.status === 200) {
                let $response = JSON.parse(this.responseText);

                // error alert
                if ($response.error) {
                    alert($response.error);
                }

                // like count update
                if ($response.result) {
                    $countElement.innerHTML = $response.count;
                }
            }
        };

        // send request
        $request.send($params);
    }


    // click like event
    $tweetsList.addEventListener('click', function(e) {
        e = e || window.event;

        let $target = e.target || e.srcElement;
        if ($target.classList.contains('like')) {
            let $tweet = $target.parentElement.parentElement;
            let $id = $tweet.getAttribute('data-id');
            let $username = $tweet.querySelector('.author a').innerHTML;
            let $link = $tweet.querySelector('h3 a').href;
            let $countElement = $tweet.querySelector('.bottom .count span');

            sendLike ($id, $username, $link, $countElement);
        }
    }, false);
}